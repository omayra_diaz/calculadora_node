//Importar la clase express

import express from 'express';


//Crear un objeto express
const app=express();
const puerto=3007;

// Crear una ruta

app.get("/bienvenida", (req, res)=>{

    res.send("Bienvenido al mundo de backend Node.");
});

app.get("/sumar", (peticion, respuesta)=>{

    let resultado=Number(peticion.query.a)+Number(peticion.query.b);

    respuesta.send(resultado.toString());
});

// resta de dos numeros 

app.get("/restar", (peticion, respuesta)=>{

    let resultado=Number(peticion.query.a)-Number(peticion.query.b);

    respuesta.send(" Respuesta "+resultado.toString());
});

// multiplicacion de dos numeros 

app.get("/multiplicar", (peticion, respuesta)=>{

    let resultado=Number(peticion.query.a)*Number(peticion.query.b);

    respuesta.send(" Respuesta "+resultado.toString());
});

// divicion de dos numeros 

app.get("/dividir", (peticion, respuesta)=>{

    if(Number(peticion.query.b)!=0){
    let resultado=Number(peticion.query.a)/Number(peticion.query.b);
    respuesta.send(" Respuesta "+resultado.toString());
}
if(Number(peticion.query.b)==0){
    respuesta.send("No se puede dividir entre 0");
}
});

//Operacion modulo
app.get("/modular", (peticion, respuesta)=>{

    let resultado=Number(peticion.query.a)%Number(peticion.query.b);

    respuesta.send(" Respuesta "+resultado.toString());
});


//Inicializar el servidor node

app.listen(puerto, ()=>{console.log("Esta funcionando el servidor.")});

